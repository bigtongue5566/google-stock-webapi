const axios = require('axios')
const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
app.get('/stock/:stockcode', (req, res) => {
    let stockCode = req.params.stockcode;
    getStockCode(stockCode).then((data)=> res.json(data));
})


function getStockCode (stockCode){
    let job = new Promise((resolve, reject) => {
        let apiUrl = `https://www.google.com/finance/getprices?q=${stockCode}&x=TPE&i=86400&p=1Y&f=d,c`;
        axios.get(apiUrl).then((result) => {
            let lines = result.data.split("\n");
            let firstLine = lines[7].split(',');
            let firstLineTime = Number(firstLine[0].substring(1));
            let firstLinePrice = firstLine[1];
            let allData = [];
            let firstData = {
                time: firstLineTime,
                price: firstLinePrice
            };
            let prices = lines.slice(8);      
            allData.push(firstData);
            let otherData = prices.forEach(l => {
                let time = firstLineTime + Number(l.split(",")[0]) * 86400;
                let price = l.split(",")[1];
                allData.push({
                    time: time,
                    price: price
                });
            });
            resolve(allData);
        });
    })
    return job;
}

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Example app listening on port ${port}`));